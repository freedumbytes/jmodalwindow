package nl.demon.shadowland.freedumbytes.swingx.gui;


import java.awt.*;
import java.net.*;

import javax.swing.*;

import nl.demon.shadowland.freedumbytes.swingx.gui.modal.*;


/**
 * <p>An extension of JModalWindow which can be used as a splash screen. </p>
 * <p>Project: JModalWindow - window-specific modality. </p>
 * <p>Copyright: Copyright (c) 2001-2006. </p>
 *
 * <p>This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version. </p>
 *
 * <p>This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. </p>
 *
 * <p>You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA </p>
 *
 * @author Jene Jasper
 * @version 2.0
 */
public class JSplashWindow extends JModalWindow
{
  private static final long serialVersionUID = 1L;

  private JImagePanel jip = null;

  /**
   * <p>Open a splash window with the supplied image. </p>
   *
   * @param image Image
   */
  public JSplashWindow(Image image)
  {
    init(new JImagePanel(image));
  }


  /**
   * <p>Open a splash window with the supplied imageIcon. </p>
   *
   * @param imageIcon ImageIcon
   */
  public JSplashWindow(ImageIcon imageIcon)
  {
    init(new JImagePanel(imageIcon));
  }


  /**
   * <p>Open a splash window with the specified image file. </p>
   *
   * @param filename String
   */
  public JSplashWindow(String filename)
  {
    init(new JImagePanel(filename));
  }


  /**
   * <p>Open a splash window with the specified image URL. </p>
   *
   * @param url URL
   */
  public JSplashWindow(URL url)
  {
    init(new JImagePanel(url));
  }


  /**
   * <p>Initialize splash window image content. </p>
   *
   * @param jip JImagePanel
   */
  private void init(JImagePanel jip)
  {
    this.jip = jip;

    getContentPane().add(jip);

    setSize(jip.getImageSize());
  }


  /**
   * <p>Set image content background color. </p>
   *
   * @param color Color
   */
  public void setBackground(Color color)
  {
    super.setBackground(color);

    if (jip != null)
    {
      jip.setBackground(color);
    }
  }
}
