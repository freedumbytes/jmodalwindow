package nl.demon.shadowland.freedumbytes.swingx.gui;


import java.awt.*;
import java.awt.event.*;
import java.net.*;

import javax.swing.*;
import javax.swing.border.*;

import nl.demon.shadowland.freedumbytes.swingx.gui.modal.*;
import nl.demon.shadowland.freedumbytes.swingx.gui.utility.*;


/**
 * <p>An extension of JModalFrame which can be used as a splash screen. </p>
 * <p>Project: JModalWindow - window-specific modality. </p>
 * <p>Copyright: Copyright (c) 2001-2006. </p>
 *
 * <p>This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version. </p>
 *
 * <p>This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. </p>
 *
 * <p>You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA </p>
 *
 * @author Jene Jasper
 * @version 2.0
 */
public class JSplashFrame extends JModalFrame
{
  private static final long serialVersionUID = 1L;

  private JPanel contentPanel;
  private Point priorDragLocation;
  private Point cursorDragAnchor;
  private int windowDragBorderDistance;

  private JImagePanel jip = null;

  /**
   * <p>Open a splash frame with the supplied image. </p>
   *
   * @param image Image
   */
  public JSplashFrame(Image image)
  {
    init(new JImagePanel(image));
  }


  /**
   * <p>Open a splash frame with the supplied imageIcon. </p>
   *
   * @param imageIcon ImageIcon
   */
  public JSplashFrame(ImageIcon imageIcon)
  {
    init(new JImagePanel(imageIcon));
  }


  /**
   * <p>Open a splash frame with the specified image file. </p>
   *
   * @param filename String
   */
  public JSplashFrame(String filename)
  {
    init(new JImagePanel(filename));
  }


  /**
   * <p>Open a splash frame with the specified image URL. </p>
   *
   * @param url URL
   */
  public JSplashFrame(URL url)
  {
    init(new JImagePanel(url));
  }


  /**
   * <p>Initialize splash frame image content. </p>
   *
   * @param jip JImagePanel
   */
  private void init(JImagePanel jip)
  {
    windowDragBorderDistance = JModalConfiguration.getWindowDragBorderDistance();

    contentPanel = new JPanel();

    contentPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
    contentPanel.setBackground(getBackground());
    contentPanel.setForeground(getForeground());

    contentPanel.setLayout(new BorderLayout()
    {
      private static final long serialVersionUID = 1L;

      public void addLayoutComponent(Component comp, Object constraints)
      {
        if (constraints == null)
        {
          constraints = BorderLayout.CENTER;
        }

        super.addLayoutComponent(comp, constraints);
      }
    });

    setContentPane(contentPanel);

    this.jip = jip;

    getContentPane().add(jip);

    setUndecorated(true);
    setSize(jip.getImageSize());

    enableEvents(AWTEvent.MOUSE_MOTION_EVENT_MASK);
  }


  /**
   * <p>Determine whether the undecorated frame should be dragged or the drag cursor should be turned on or off. </p>
   *
   * @param mouseEvent the event that triggered this method call.
   */
  protected void processMouseMotionEvent(MouseEvent mouseEvent)
  {
    switch (mouseEvent.getID())
    {
      case MouseEvent.MOUSE_MOVED:
        checkDragZone(mouseEvent);
        break;
      case MouseEvent.MOUSE_DRAGGED:
        dragWindow(mouseEvent);
        break;
      default:
        break;
    }

    super.processMouseMotionEvent(mouseEvent);
  }


  /**
   * <p>Determine based on the mouse position if the drag cursor should be turned on or off. </p>
   *
   * @param mouseEvent the event that triggered this method call.
   */
  private void checkDragZone(MouseEvent mouseEvent)
  {
    if (!JModalConfiguration.isWindowDraggingEnabled())
    {
      return;
    }

    priorDragLocation = mouseEvent.getPoint();
    cursorDragAnchor = null;

    if ((mouseEvent.getX() < windowDragBorderDistance) || (mouseEvent.getX() >= getWidth() - windowDragBorderDistance))
    {
      this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
    }
    else if ((mouseEvent.getY() < windowDragBorderDistance) || (mouseEvent.getY() >= getHeight() - windowDragBorderDistance))
    {
      this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
    }
    else
    {
      this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }
  }


  /**
   * <p>Determine based on the mouse position the new frame location. </p>
   *
   * <p>Note: It is no longer possible to drag the frame off the screen. </p>
   *
   * @param mouseEvent the event that triggered this method call.
   */
  private void dragWindow(MouseEvent mouseEvent)
  {
    if (!JModalConfiguration.isWindowDraggingEnabled())
    {
      return;
    }

    Point cursorRelativePosition = mouseEvent.getPoint();

    if (cursorDragAnchor == null)
    {
      cursorDragAnchor = cursorRelativePosition;
    }

    int x;
    int y;
    int deltaX;
    int deltaY;

    if (this.getCursor().equals(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR)))
    {
      if (priorDragLocation == null)
      {
        priorDragLocation = mouseEvent.getPoint();
      }
      else
      {
        deltaX = mouseEvent.getX() - priorDragLocation.x;
        deltaY = mouseEvent.getY() - priorDragLocation.y;

        deltaX += cursorRelativePosition.x - cursorDragAnchor.x;
        deltaY += cursorRelativePosition.y - cursorDragAnchor.y;

        x = getX() + deltaX;
        y = getY() + deltaY;

        if ((x != getX()) || (y != getY()))
        {
          Utils.keepWindowPartiallyOnScreen(this, x, y);
        }

        priorDragLocation = null;
      }
    }
  }


  /**
   * <p>Paint a border around the undecorated frame. </p>
   *
   * @param gfx window <code>Graphics</code>.
   */
  public void paint(Graphics gfx)
  {
    gfx.draw3DRect(0, 0, getWidth(), getHeight(), false);

    super.paint(gfx);
  }


  /**
   * <p>Set image content background color. </p>
   *
   * @param color Color
   */
  public void setBackground(Color color)
  {
    super.setBackground(color);

    if (contentPanel != null)
    {
      contentPanel.setBackground(color);
    }

    if (jip != null)
    {
      jip.setBackground(color);
    }
  }


  /**
   * <p>Set image content foreground color. </p>
   *
   * @param color foreground color.
   */
  public void setForeground(Color color)
  {
    super.setForeground(color);

    if (contentPanel != null)
    {
      contentPanel.setForeground(color);
    }
  }
}
