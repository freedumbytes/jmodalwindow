package nl.demon.shadowland.freedumbytes.swingx.gui.modal;


import java.awt.*;

import javax.swing.*;


/**
 * <p>Custom EventQueue which enables halting the calling thread in the same way as the JDialog does. </p>
 * <p>Project: JModalWindow - window-specific modality. </p>
 * <p>Copyright: Copyright (c) 2001-2006. </p>
 *
 * <p>This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version. </p>
 *
 * <p>This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. </p>
 *
 * <p>You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA </p>
 *
 * @author Jene Jasper
 * @version 2.0
 */
public class JModalEventQueue extends EventQueue
{
  /**
   * <p>Dispatch the next event on the queue through the normally protected {@link #dispatchEvent(AWTEvent) dispatchEvent} method. </p>
   */
  public void dispatchNextEventOnQueue()
  {
    try
    {
      dispatchEvent(getNextEvent());
    }
    catch (InterruptedException ie)
    {
      ie.printStackTrace(System.out);
      Thread.currentThread().interrupt();
    }
  }


  /**
   * <p>Pump events until specified window is closed. And thus simulate halting even the event dispatcher thread. </p>
   *
   * @param window Window
   */
  public final void waitForClose(Window window)
  {
    while (window.isVisible() && window.isDisplayable())
    {
      dispatchNextEventOnQueue();
    }
  }


  /**
   * <p>Pump events until specified internal frame is closed. And thus simulate halting even the event dispatcher thread. </p>
   *
   * @param iframe JInternalFrame
   */
  public final void waitForClose(JInternalFrame iframe)
  {
    while (iframe.isVisible() && iframe.isDisplayable() && !iframe.isClosed())
    {
      dispatchNextEventOnQueue();
    }
  }
}
