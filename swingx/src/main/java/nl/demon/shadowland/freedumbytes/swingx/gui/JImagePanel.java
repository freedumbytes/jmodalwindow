package nl.demon.shadowland.freedumbytes.swingx.gui;


import java.awt.*;
import java.net.*;

import javax.swing.*;


/**
 * <p>JPanel that is used to display an image in several ways. </p>
 * <p>Project: JModalWindow - window-specific modality. </p>
 * <p>Copyright: Copyright (c) 2001-2006. </p>
 *
 * <p>This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version. </p>
 *
 * <p>This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. </p>
 *
 * <p>You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA </p>
 *
 * @author Jene Jasper
 * @version 2.0
 */
public class JImagePanel extends JPanel
{
  private static final long serialVersionUID = 1L;

  /**
   * <p>Place the background image at the top left corner of the panel. </p>
   */
  public static final int MODE_WALLPAPER = 0;

  /**
   * <p>Place the background image in the center of the panel. </p>
   */
  public static final int MODE_CENTER = 1;

  /**
   * <p>Repeat the background image like tiles across the panel. </p>
   */
  public static final int MODE_TILES = 2;

  /**
   * <p>Stretch or shrink the background image to fit the panel. </p>
   */
  public static final int MODE_FULL = 3;

  private boolean fixWidth = false;
  private boolean fixHeight = false;
  private Image image = null;
  private Image imageBuffer = null;
  private Graphics imageBufferGfx = null;
  private boolean imageLoaded = false;
  private String filename;
  private URL url;
  private boolean imageOn = true;
  private int mode = MODE_WALLPAPER;

  /**
   * <p>Construct an image panel without specifying the background image yet. </p>
   */
  public JImagePanel()
  {
    super();
  }


  /**
   * <p>Construct an image panel with the specified background image. </p>
   *
   * @param image Image
   */
  public JImagePanel(Image image)
  {
    super();

    this.image = image;
    this.filename = null;
    this.url = null;
    imageLoaded = true;
  }


  /**
   * <p>Construct an image panel with the specified background image
   * and whether the width and/or height of this image should be used as the preferredSize. </p>
   *
   * @param image Image
   * @param fixWidth boolean
   * @param fixHeight boolean
   */
  public JImagePanel(Image image, boolean fixWidth, boolean fixHeight)
  {
    this(image);

    this.fixWidth = fixWidth;
    this.fixHeight = fixHeight;
  }


  /**
   * <p>Construct an image panel with the specified background imageIcon. </p>
   *
   * @param imageIcon ImageIcon
   */
  public JImagePanel(ImageIcon imageIcon)
  {
    super();

    this.image = imageIcon.getImage();
    this.filename = null;
    this.url = null;
    imageLoaded = true;
  }


  /**
   * <p>Construct an image panel with the specified background imageIcon
   * and whether the width and/or height of this image should be used as the preferredSize. </p>
   *
   * @param imageIcon ImageIcon
   * @param fixWidth boolean
   * @param fixHeight boolean
   */
  public JImagePanel(ImageIcon imageIcon, boolean fixWidth, boolean fixHeight)
  {
    this(imageIcon);

    this.fixWidth = fixWidth;
    this.fixHeight = fixHeight;
  }


  /**
   * <p>Construct an image panel with the specified background image file. </p>
   *
   * @param filename String
   */
  public JImagePanel(String filename)
  {
    this(filename, false);
  }


  /**
   * <p>Construct an image panel with the specified background image file.
   * Depending on the value of reloadImage image object sharing is used. </p>
   *
   * @param filename String
   * @param reloadImage boolean
   */
  public JImagePanel(String filename, boolean reloadImage)
  {
    super();

    this.filename = filename;
    this.url = null;
    loadImage(reloadImage);
  }


  /**
   * <p>Construct an image panel with the specified background image file
   * and whether the width and/or height of this image should be used as the preferredSize.
   * Depending on the value of reloadImage image object sharing is used. </p>
   *
   * @param filename String
   * @param reloadImage boolean
   * @param fixWidth boolean
   * @param fixHeight boolean
   */
  public JImagePanel(String filename, boolean reloadImage, boolean fixWidth, boolean fixHeight)
  {
    this(filename, reloadImage);

    this.fixWidth = fixWidth;
    this.fixHeight = fixHeight;
  }


  /**
   * <p>Construct an image panel with the specified background image URL. </p>
   *
   * @param url URL
   */
  public JImagePanel(URL url)
  {
    this(url, false);
  }


  /**
   * <p>Construct an image panel with the specified background image URL.
   * Depending on the value of reloadImage image object sharing is used. </p>
   *
   * @param url URL
   * @param reloadImage boolean
   */
  public JImagePanel(URL url, boolean reloadImage)
  {
    super();

    this.filename = null;
    this.url = url;
    loadImage(reloadImage);
  }


  /**
   * <p>Construct an image panel with the specified background image URL
   * and whether the width and/or height of this image should be used as the preferredSize.
   * Depending on the value of reloadImage image object sharing is used. </p>
   *
   * @param url URL
   * @param reloadImage boolean
   * @param fixWidth boolean
   * @param fixHeight boolean
   */
  public JImagePanel(URL url, boolean reloadImage, boolean fixWidth, boolean fixHeight)
  {
    this(url, reloadImage);

    this.fixWidth = fixWidth;
    this.fixHeight = fixHeight;
  }


  /**
   * <p>Specify a new background image. </p>
   *
   * @param image Image
   */
  public void setImage(Image image)
  {
    this.image = image;
    this.filename = null;
    this.url = null;
    imageLoaded = true;

    repaint();
  }


  /**
   * <p>Specify a new background imageIcon. </p>
   *
   * @param icon ImageIcon
   */
  public void setImage(ImageIcon icon)
  {
    this.image = icon.getImage();
    imageLoaded = true;
    this.filename = null;
    this.url = null;

    repaint();
  }


  /**
   * <p>Specify a new background image file. </p>
   *
   * @param filename String
   */
  public void setImage(String filename)
  {
    setImage(filename, false);
  }


  /**
   * <p>Specify a new background image file.
   * Depending on the value of reloadImage image object sharing is used. </p>
   *
   * @param filename String
   * @param reloadImage boolean
   */
  public void setImage(String filename, boolean reloadImage)
  {
    this.filename = filename;
    this.url = null;
    loadImage(reloadImage);
  }


  /**
   * <p>Specify a new background image URL. </p>
   *
   * @param url URL
   */
  public void setImage(URL url)
  {
    setImage(url, false);
  }


  /**
   * <p>Specify a new background image URL.
   * Depending on the value of reloadImage image object sharing is used. </p>
   *
   * @param url URL
   * @param reloadImage boolean
   */
  public void setImage(URL url, boolean reloadImage)
  {
    this.filename = null;
    this.url = url;
    loadImage(reloadImage);
  }


  /**
   * <p>Load the specified background image file or URL.
   * Depending on the value of reloadImage image object sharing is used. </p>
   *
   * @param reloadImage boolean
   */
  private void loadImage(boolean reloadImage)
  {
    MediaTracker tracker = new MediaTracker(this);

    imageLoaded = false;

    repaint();

    try
    {
      if (filename != null)
      {
        if (reloadImage)
        {
          image = this.getToolkit().createImage(filename);
        }
        else
        {
          image = this.getToolkit().getImage(filename);
        }
      }
      else
      {
        if (reloadImage)
        {
          image = this.getToolkit().createImage(url);
        }
        else
        {
          image = this.getToolkit().getImage(url);
        }
      }

      try
      {
        tracker.addImage(image, 0);
        tracker.waitForAll();

        imageLoaded = !tracker.isErrorAny();
      }
      catch (InterruptedException ie)
      {
        System.out.println("Loading background image " + ((filename != null) ? filename : url.toString()) + " is interrupted.");
        Thread.currentThread().interrupt();
      }
    }
    catch (Exception e)
    {
      System.out.println("Can't load background image: " + ((filename != null) ? filename : url.toString()));
    }

    repaint();
  }


  /**
   * <p>Disable the background image painting. </p>
   */
  public void turnImageOff()
  {
    imageOn = false;
  }


  /**
   * <p>Enable the background image painting. </p>
   */
  public void turnImageOn()
  {
    imageOn = true;
  }


  /**
   * <p>Set the mode in which the background image is painted. </p>
   *
   * @param mode int
   */
  public void setMode(int mode)
  {
    this.mode = mode;
  }


  /**
   * <p>Paint the background image in the specified mode. </p>
   *
   * @param gfx Graphics
   */
  public void paint(Graphics gfx)
  {
    int x;
    int y;

    if (imageBuffer == null)
    {
      imageBuffer = createImage(getWidth(), getHeight());
      imageBufferGfx = imageBuffer.getGraphics();
    }

    if (imageLoaded && imageOn)
    {
      imageBufferGfx.setColor(getBackground());
      imageBufferGfx.fillRect(0, 0, getWidth(), getHeight());

      switch (mode)
      {
        case MODE_FULL:
          imageBufferGfx.drawImage(image, 0, 0, getWidth(), getHeight(), this);

          break;
        case MODE_CENTER:
          Rectangle panelBounds = getBounds();
          x = (int)(panelBounds.getX() + (panelBounds.getWidth() / 2.0) - (image.getWidth(this) / 2.0));
          y = (int)(panelBounds.getY() + (panelBounds.getHeight() / 2.0) - (image.getHeight(this) / 2.0));

          imageBufferGfx.drawImage(image, x, y, this);

          break;
        case MODE_TILES:
          for (x = 0; x < getWidth(); x += image.getWidth(this))
          {
            for (y = 0; y < getHeight(); y += image.getHeight(this))
            {
              imageBufferGfx.drawImage(image, x, y, this);
            }
          }

          break;
        case MODE_WALLPAPER:
        default:
          imageBufferGfx.drawImage(image, 0, 0, this);

          break;
      }

      gfx.drawImage(imageBuffer, 0, 0, this);

      if (this.getBorder() != null)
      {
        this.getBorder().paintBorder(this, gfx, 0, 0, getWidth(), getHeight());
      }
    }
    else
    {
      imageBufferGfx.clearRect(0, 0, getWidth(), getHeight());

      gfx.drawImage(imageBuffer, 0, 0, this);
    }

    paintChildren(gfx);
  }


  /**
   * @deprecated As of JDK version 1.1,
   * replaced by <code>setBounds(int, int, int, int)</code>.
   *
   * @param x int
   * @param y int
   * @param width int
   * @param height int
   */
  public void reshape(int x, int y, int width, int height)
  {
    imageBuffer = null;

    super.reshape(x, y, width, height);
  }


  /**
   * <p>Returns the current width of this component or if width is specified as fixed the width of the background image. </p>
   *
   * @return int
   */
  public int getWidth()
  {
    if (fixWidth)
    {
      return image.getWidth(this);
    }
    else
    {
      return super.getWidth();
    }
  }


  /**
   * <p>Returns the current height of this component or if height is specified as fixed the height of the background image. </p>
   *
   * @return int
   */
  public int getHeight()
  {
    if (fixHeight)
    {
      return image.getHeight(this);
    }
    else
    {
      return super.getHeight();
    }
  }


  /**
   *  <p>If the minimum size has been set to a non-null value just returns it.
   * If the UI delegate's getMinimumSize method returns a non-null value then return that;
   * otherwise defer to the component's layout manager.
   * If the image witdh is specified as the preferredSize the above value is overruled. </p>
   *
   * @return Dimension
   */
  public Dimension getMinimumSize()
  {
    Dimension dim = super.getMinimumSize();

    if (fixWidth)
    {
      dim.width = getWidth();
    }

    if (fixHeight)
    {
      dim.height = getHeight();
    }

    return dim;
  }


  /**
   * <p>If the maximum size has been set to a non-null value just returns it.
   * If the UI delegate's getMaximumSize method returns a non-null value then return that;
   * otherwise defer to the component's layout manager.
   * If the image height is specified as the preferredSize the above value is overruled. </p>
   *
   * @return Dimension
   */
  public Dimension getMaximumSize()
  {
    Dimension dim = super.getMaximumSize();

    if (fixWidth)
    {
      dim.width = getWidth();
    }

    if (fixHeight)
    {
      dim.height = getHeight();
    }

    return dim;
  }


  /**
   * <p>If the preferredSize has been set to a non-null value just returns it.
   * If the UI delegate's getPreferredSize method returns a non null value then return that;
   * otherwise defer to the component's layout manager.
   * If the image witdh and/or height is specified as the preferredSize the above values are overruled. </p>
   *
   * @return Dimension
   */
  public Dimension getPreferredSize()
  {
    Dimension dim = super.getPreferredSize();

    if (fixWidth)
    {
      dim.width = getWidth();
    }

    if (fixHeight)
    {
      dim.height = getHeight();
    }

    return dim;
  }


  /**
   * <p>Returns the dimension of the background image. </p>
   *
   * @return Dimension
   */
  public Dimension getImageSize()
  {
    return new Dimension(image.getWidth(this), image.getHeight(this));
  }
}
