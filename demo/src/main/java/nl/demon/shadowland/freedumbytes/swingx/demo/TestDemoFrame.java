package nl.demon.shadowland.freedumbytes.swingx.demo;


import java.awt.*;

import javax.swing.*;

import nl.demon.shadowland.freedumbytes.swingx.gui.modal.*;


/**
 * <p>The test JModalFrame used in the {@link TestModalFrame} demo. </p>
 * <p>Project: JModalWindow - window-specific modality. </p>
 * <p>Copyright: Copyright (c) 2001-2006. </p>
 *
 * <p>This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version. </p>
 *
 * <p>This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. </p>
 *
 * <p>You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA </p>
 *
 * @author Jene Jasper
 * @version 2.0
 */
public class TestDemoFrame extends JModalFrame
{
  private static final long serialVersionUID = 1L;


  public TestDemoFrame(String title, String description)
  {
    super(title);

    init(title, description);
  }


  private void init(String title, String description)
  {
    setSize(400, 150);
    setMinSize(400, 150);
    getContentPane().setLayout(new GridBagLayout());

    getContentPane().setBackground(Color.blue);
    getContentPane().setForeground(Color.cyan);

    GridBagConstraints gbc = new GridBagConstraints();

    gbc.gridx = 0;
    gbc.gridy = GridBagConstraints.RELATIVE;
    gbc.insets = new Insets(3, 3, 3, 3);
    gbc.fill = GridBagConstraints.HORIZONTAL;
    getContentPane().add(createLabel(title + description), gbc);
  }


  private static JLabel createLabel(String text)
  {
    JLabel label = new JLabel(text);
    label.setForeground(Color.yellow);
    label.setBackground(Color.blue);

    return label;
  }
}
