package nl.demon.shadowland.freedumbytes.swingx.demo;


import java.awt.*;
import java.awt.event.*;
import java.net.*;

import javax.swing.*;

import nl.demon.shadowland.freedumbytes.swingx.gui.*;
import nl.demon.shadowland.freedumbytes.swingx.gui.modal.*;
import nl.demon.shadowland.freedumbytes.swingx.gui.utility.*;


/**
 * <p>The demo program for the <a href="https://freedumbytes.gitlab.io/jmodalwindow.xhtml">JModalWindow project</a>. </p>
 * <p>Project: JModalWindow - window-specific modality. </p>
 * <p>Copyright: Copyright (c) 2001-2006. </p>
 *
 * <p>This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version. </p>
 *
 * <p>This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. </p>
 *
 * <p>You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA </p>
 *
 * @author Jene Jasper
 * @version 2.0
 */
public class TestModalFrame extends JModalFrame
{
  private static final long serialVersionUID = 1L;

  private static final Color BACKGROUND_COLOR = new Color(48, 48, 152);
  private static final String PARAM_MINIMAL = "-minimal";

  private static int modalSubFrameCount = 0;
  private static int normalIFrameCount = 0;
  private static int modalIFrameCount = 0;
  private static Window secondDemoFrame;

  public TestModalFrame(String title)
  {
    super(title);

    init();
  }


  public TestModalFrame(Window owner, Component returnFocus, String title)
  {
    super(owner, returnFocus, title);

    init();
  }


  public TestModalFrame(Window owner, Component returnFocus, String title, GraphicsConfiguration gc)
  {
    super(owner, returnFocus, title, gc);

    init();
  }


  private void init()
  {
    setSize(600, 325);
    setMinSize(600, 325);
    getContentPane().setLayout(new GridBagLayout());

    getContentPane().setBackground(Color.blue);
    getContentPane().setForeground(Color.cyan);

    GridBagConstraints gbc = new GridBagConstraints();

    final JButton jbNormalWindow = createJButton("Open normal window");
    jbNormalWindow.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        jbNormalWindow.setEnabled(false);
        JModalWindow jmw = createNormalWindow(jbNormalWindow);

        jmw.addWindowListener(new WindowAdapter()
        {
          public void windowClosed(WindowEvent windowEvent)
          {
            jbNormalWindow.setEnabled(true);
          }
        });

        jmw.setVisible(true);
      }
    });

    final JButton jbModalWindow = createJButton("Open modal window");
    jbModalWindow.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        createModalWindow(jbModalWindow, false).setVisible(true);
      }
    });

    final JButton jbModalWindow2 = createJButton("Open modal window");
    jbModalWindow2.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        JModalWindow modalWindow = createModalWindow(jbModalWindow2, false);
        modalWindow.addAdditionalModalToWindow(secondDemoFrame);
        modalWindow.setVisible(true);
      }
    });

    final JButton jbModalWindows = createJButton("Open 2 modal windows");
    jbModalWindows.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        createModalWindow(jbModalWindows, true).setVisible(true);
      }
    });

    final JButton jbModalFrame = createJButton("Open modal frame");
    jbModalFrame.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        JModalFrame jmf = createModalFrame(jbModalFrame);
        jmf.setVisible(true);

        if (JModalConfiguration.simulateWaitOnEDT())
        {
          jmf.waitForClose();
          System.out.println("Frame was just closed!");
        }
      }
    });

    final JButton jbFrame = createJButton("Open frame with internal frames");
    jbFrame.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        createModalFrameWithInternalFrames(jbFrame).setVisible(true);
      }
    });

    final JButton jbModalDialog = createJButton("Open modal dialog");
    jbModalDialog.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        createModalDialog(jbModalDialog).setVisible(true);
      }
    });

    final JButton jbCloseFrame = createJButton("Close this frame");
    jbCloseFrame.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        TestModalFrame.this.dispose();
      }
    });

    final JCheckBox jcbKeepModalToWindowInFront = new JCheckBox("keepModalToWindowInFront",
                                                                JModalConfiguration.keepModalToWindowInFront());
    jcbKeepModalToWindowInFront.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        if (jcbKeepModalToWindowInFront.isSelected())
        {
          JModalConfiguration.enableKeepModalToWindowInFront();
        }
        else
        {
          JModalConfiguration.disableKeepModalToWindowInFront();
        }
      }
    });

    gbc.gridx = 0;
    gbc.gridy = GridBagConstraints.RELATIVE;
    gbc.insets = new Insets(3, 3, 3, 3);
    gbc.fill = GridBagConstraints.HORIZONTAL;
    getContentPane().add(jbNormalWindow, gbc);
    getContentPane().add(jbModalWindow, gbc);
    getContentPane().add(jbModalWindow2, gbc);
    getContentPane().add(jbModalWindows, gbc);
    getContentPane().add(jbModalFrame, gbc);
    getContentPane().add(jbFrame, gbc);
    getContentPane().add(jbModalDialog, gbc);

    if (modalSubFrameCount == 0)
    {
      getContentPane().add(jcbKeepModalToWindowInFront, gbc);
    }

    getContentPane().add(jbCloseFrame, gbc);

    gbc.gridx = 1;
    getContentPane().add(createLabel("A) Open a normal window in the center of this frame."), gbc);
    getContentPane().add(createLabel("B) Open a modal window relative to this button."), gbc);
    getContentPane().add(createLabel("C) Open a modal window that also blocks second demo frame."), gbc);
    getContentPane().add(createLabel("D) Open 2 modal windows relative and center of the screen."), gbc);
    getContentPane().add(createLabel("E) Open a modal frame in the center of the screen."), gbc);
    getContentPane().add(createLabel("F) Open a frame with modal internal frames."), gbc);
    getContentPane().add(createLabel("G) Open a modal dialog in the center of the screen."), gbc);

    if (modalSubFrameCount == 0)
    {
      getContentPane().add(createLabel("K) Enable frame/window like behavior between frame/frame."), gbc);
    }

    getContentPane().add(createLabel("Z) Close this frame and end the test."), gbc);
  }


  private JModalWindow createNormalWindow(Component cmp)
  {
    final JModalWindow jmw = new JModalWindow(Utils.windowForComponent(cmp), cmp, false);
    jmw.getContentPane().setBackground(Color.green);
    jmw.getContentPane().setForeground(Color.white);
    jmw.getContentPane().setLayout(new BorderLayout());

    final JButton jbClose = createJButton("Close normal window");
    jbClose.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        jmw.dispose();
      }
    });

    jmw.getContentPane().add(jbClose, BorderLayout.CENTER);
    jmw.setSize(200, 200);
    jmw.centerOfOwner();

    return jmw;
  }


  private JModalWindow createModalWindow(Component cmp, boolean multiple)
  {
    if (multiple)
    {
      GridBagConstraints gbc = new GridBagConstraints();

      final JModalWindow jmw0 = new JModalWindow(Utils.windowForComponent(cmp));
      jmw0.getContentPane().setBackground(Color.red);
      jmw0.getContentPane().setForeground(Color.white);
      jmw0.getContentPane().setLayout(new GridBagLayout());

      final JButton jbModalFrame = createJButton("Open modal frame");
      jbModalFrame.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent ae)
        {
          JModalFrame jmf = createModalFrame(jbModalFrame);
          jmf.setVisible(true);

          if (JModalConfiguration.simulateWaitOnEDT())
          {
            jmf.waitForClose();
            System.out.println("Frame was just closed!");
          }
        }
      });

      final JButton jbClose0 = createJButton("Close modal window (without returnFocus)");
      jbClose0.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent ae)
        {
          jmw0.dispose();
        }
      });

      gbc.gridx = 0;
      gbc.gridy = GridBagConstraints.RELATIVE;
      gbc.insets = new Insets(3, 3, 3, 3);
      gbc.fill = GridBagConstraints.HORIZONTAL;

      jmw0.getContentPane().add(jbModalFrame, gbc);
      jmw0.getContentPane().add(jbClose0, gbc);
      jmw0.setSize(325, 175);
      jmw0.centerOfScreen();
      jmw0.setVisible(true);
    }

    Window owner = Utils.windowForComponent(cmp);
    Component parent = Utils.parentForComponentRootPane(cmp);

    final JModalWindow jmw = new JModalWindow(owner, cmp, !(parent instanceof JModalInternalFrame));

    if (parent instanceof JModalInternalFrame)
    {
      jmw.addAdditionalModalToComponent((JModalInternalFrame)parent);
    }

    jmw.getContentPane().setBackground(Color.red);
    jmw.getContentPane().setForeground(Color.white);
    jmw.getContentPane().setLayout(new GridBagLayout());

    final JButton jbClose = createJButton("Close modal window (with returnFocus)");
    jbClose.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        jmw.dispose();
      }
    });

    jmw.getContentPane().add(jbClose, new GridBagConstraints());
    jmw.setSize(300, 200);
    jmw.relativeToOwnerChild(cmp);

    return jmw;
  }


  private JModalFrame createModalFrame(Component cmp)
  {
    final int lastSubFrameCount = modalSubFrameCount;
    incrementSubFrameCount();

    JModalFrame subFrame;
    Window owner = Utils.windowForComponent(cmp);
    Component parent = Utils.parentForComponentRootPane(cmp);

    if (parent instanceof JModalInternalFrame)
    {
      subFrame = new TestModalFrame(null, null, "Test Modal SubFrame #" + modalSubFrameCount,
                                    Utils.windowForComponent(cmp).getGraphicsConfiguration());
      subFrame.addAdditionalModalToComponent((JModalInternalFrame)parent);
    }
    else
    {
      subFrame = new TestModalFrame(owner, cmp, "Test Modal SubFrame #" + modalSubFrameCount,
                                    Utils.windowForComponent(cmp).getGraphicsConfiguration());
    }

    subFrame.addWindowListener(new WindowAdapter()
    {
      public void windowClosing(WindowEvent we)
      {
        resetSubFrameCount(lastSubFrameCount);
      }


      public void windowClosed(WindowEvent we)
      {
        resetSubFrameCount(lastSubFrameCount);
      }
    });

    subFrame.centerOfScreen();

    return subFrame;
  }


  private Window createModalFrameWithInternalFrames(Component cmp)
  {
    final int lastSubFrameCount = modalSubFrameCount;
    incrementSubFrameCount();

    JModalFrame subFrame = new JModalFrame(Utils.windowForComponent(cmp), cmp, "Test Modal SubFrame #" + modalSubFrameCount, false,
                                           Utils.windowForComponent(cmp).getGraphicsConfiguration());
//    JModalWindow subFrame = createNormalWindow(cmp);

    subFrame.addWindowListener(new WindowAdapter()
    {
      public void windowClosing(WindowEvent we)
      {
        resetSubFrameCount(lastSubFrameCount);
      }


      public void windowClosed(WindowEvent we)
      {
        resetSubFrameCount(lastSubFrameCount);
      }
    });

    subFrame.setSize(600, 480);
    subFrame.centerOfScreen();

    JMenuBar menubar = new JMenuBar();
    JMenu help = new JMenu("Help");
    JMenuItem about = new JMenuItem("About");
    help.add(about);
    menubar.add(help);
    subFrame.setJMenuBar(menubar);

    final JDesktopPane desktop = new JDesktopPane();
    subFrame.getContentPane().add(desktop);

    createNormalInternalFrame(desktop, subFrame).setVisible(true);

    return subFrame;
  }


  private static synchronized void incrementSubFrameCount()
  {
    modalSubFrameCount++;
  }


  private static synchronized void resetSubFrameCount(final int lastSubFrameCount)
  {
    modalSubFrameCount = lastSubFrameCount;
  }


  public JModalInternalFrame createNormalInternalFrame(final JDesktopPane desktop, final Window parentWindow)
  {
    incrementNormalIFrameCount();

    final JModalInternalFrame jif = new JModalInternalFrame("Normal IFrame #" + normalIFrameCount, true, true, true, true);
    jif.getContentPane().setLayout(new GridBagLayout());
    jif.getContentPane().setBackground(desktop.getBackground());
    jif.getContentPane().setForeground(desktop.getForeground());

    GridBagConstraints gbc = new GridBagConstraints();

    final JTextField tfInput = new JTextField();
    tfInput.setPreferredSize(new Dimension(100, 20));

    final JLabel tfLabel = createLabel("Just an input field:");
    tfLabel.setForeground(desktop.getForeground());

    final JButton jbNormalIFrame = createJButton("Open a normal internal frame");
    jbNormalIFrame.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        createNormalInternalFrame(desktop, parentWindow).setVisible(true);
      }
    });

    final JButton jbModalIFrame = createJButton("Open a modal internal frame");
    jbModalIFrame.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        createModalInternalFrame(desktop, jif, jbModalIFrame, false).setVisible(true);
      }
    });

    final JButton jbModalIDialog = createJButton("Open a modal internal dialog");
    jbModalIDialog.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        JModalInternalFrame dialog = createModalInternalFrame(desktop, jif, jbModalIDialog, true);
        dialog.setVisible(true);

        if (JModalConfiguration.simulateWaitOnEDT())
        {
          dialog.waitForClose();
          System.out.println("Dialog was just closed!");
        }
      }
    });

    final JButton jbCloseFrame = createJButton("Close internal frame #" + normalIFrameCount);
    jbCloseFrame.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        jif.dispose();
      }
    });

    final JButton jbCloseParentFrame = createJButton("Close parent frame");
    jbCloseParentFrame.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        if (parentWindow instanceof CloseBlocker && !((CloseBlocker) parentWindow).isClosable())
        {
          return;
        }

        parentWindow.dispose();
      }
    });

    jif.setSize(400, 320);
    jif.setMinSize(400, 190);

    gbc.gridx = 1;
    gbc.gridy = GridBagConstraints.RELATIVE;
    gbc.insets = new Insets(3, 3, 3, 3);
    gbc.fill = GridBagConstraints.HORIZONTAL;
    jif.getContentPane().add(tfInput, gbc);
    jif.getContentPane().add(new JLabel(), gbc);
    jif.getContentPane().add(new JLabel(), gbc);
    jif.getContentPane().add(jbCloseFrame, gbc);
    jif.getContentPane().add(jbCloseParentFrame, gbc);

    gbc.gridx = 0;
    JPanel panel = new JPanel();
    panel.setLayout(new BorderLayout());
    panel.add(tfLabel, BorderLayout.EAST);
    panel.setOpaque(false);
    jif.getContentPane().add(panel, gbc);
    jif.getContentPane().add(jbNormalIFrame, gbc);
    jif.getContentPane().add(jbModalIFrame, gbc);
    jif.getContentPane().add(jbModalIDialog, gbc);

    desktop.add(jif);
    jif.centerOfDesktop();

    return jif;
  }


  private static synchronized void incrementNormalIFrameCount()
  {
    normalIFrameCount++;
  }


  public JModalInternalFrame createModalInternalFrame(JDesktopPane desktop, JModalInternalFrame owner, Component returnFocus,
                                                      boolean dialog)
  {
    incrementModalIFrameCount();

    JModalInternalFrame iframe;

    if (dialog)
    {
      iframe = new JModalInternalDialog(owner, returnFocus, "Modal IDialog #" + modalIFrameCount, true, true, true, true);
    }
    else
    {
      iframe = new JModalInternalFrame(owner, returnFocus, "Modal IFrame #" + modalIFrameCount, true, true, true, true);
    }

    final JModalInternalFrame jif = iframe;
    jif.getContentPane().setLayout(new GridBagLayout());
    jif.getContentPane().setBackground(desktop.getBackground());
    jif.getContentPane().setForeground(desktop.getForeground());

    GridBagConstraints gbc = new GridBagConstraints();

    final JButton jbOpenModalWindow = createJButton("Open modal window");
    jbOpenModalWindow.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        createModalWindow(jbOpenModalWindow, false).setVisible(true);
      }
    });

    final JButton jbOpenModalFrame = createJButton("Open modal frame");
    jbOpenModalFrame.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        JModalFrame jmf = createModalFrame(jbOpenModalFrame);
        jmf.setVisible(true);

        if (JModalConfiguration.simulateWaitOnEDT())
        {
          jmf.waitForClose();
          System.out.println("Frame was just closed!");
        }
      }
    });

    final JButton jbModalDialog = createJButton("Open modal dialog");
    jbModalDialog.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        createModalDialog(jbModalDialog).setVisible(true);
      }
    });

    final JButton jbCloseFrame = createJButton("Close modal internal " + (dialog ? "dialog" : "frame") + " #" + modalIFrameCount);
    jbCloseFrame.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        jif.dispose();
      }
    });

    jif.setSize(300, 170);
    jif.setMinSize(230, 160);

    gbc.gridx = 1;
    gbc.gridy = GridBagConstraints.RELATIVE;
    gbc.insets = new Insets(3, 3, 3, 3);
    gbc.fill = GridBagConstraints.NONE;
    jif.getContentPane().add(jbOpenModalWindow, gbc);
    jif.getContentPane().add(jbOpenModalFrame, gbc);
    jif.getContentPane().add(jbModalDialog, gbc);
    jif.getContentPane().add(jbCloseFrame, gbc);

    desktop.add(jif);

    if (modalIFrameCount % 3 == 0)
    {
      jif.centerOfDesktop();
    }
    else if (modalIFrameCount % 3 == 1)
    {
      jif.centerOfOwner();
    }
    else if (modalIFrameCount % 3 == 2)
    {
      jif.relativeToOwnerChild(returnFocus);
    }

    return jif;
  }


  private static synchronized void incrementModalIFrameCount()
  {
    modalIFrameCount++;
  }


  private JModalDialog createModalDialog(Component cmp)
  {
    final JModalDialog dialog = new JModalDialog(Utils.windowForComponent(cmp), cmp,
                                                 Utils.windowForComponent(cmp).getGraphicsConfiguration());

    dialog.getContentPane().setBackground(Color.red);
    dialog.getContentPane().setForeground(Color.white);
    dialog.getContentPane().setLayout(new GridBagLayout());

    final JButton jbClose = createJButton("Close modal window (with returnFocus)");
    jbClose.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        dialog.dispose();
      }
    });

    dialog.getContentPane().add(jbClose, new GridBagConstraints());
    dialog.setSize(300, 200);
    dialog.centerOfScreen();

    return dialog;
  }


  private static JLabel createLabel(String text)
  {
    JLabel label = new JLabel(text);
    label.setForeground(Color.yellow);
    label.setBackground(Color.blue);

    return label;
  }


  private static JButton createJButton(String text)
  {
    return new JButton(text);
  }


  public static void main(String[] args)
  {
    try
    {
      JModalConfiguration.enableWaitOnEDT();
    }
    catch (SecurityException se)
    {
      se.printStackTrace(System.out);
    }

//    JModalConfiguration.setBlurColor(Color.black);

    if ((args.length > 0) && args[0].equals(PARAM_MINIMAL))
    {
      System.out.println("Running in minimal mode.");
      JModalConfiguration.setBlurStyleNone();
      JModalConfiguration.disableBusyCursor();
      JModalConfiguration.disableIconifyForBlockedInternalFrame();
      UIManager.put("swingx.frame.icon", "nl/demon/shadowland/freedumbytes/swingx/demo/Mondriaan.gif");
    }

    UIManager.put("swingx.iframe.icon", "nl/demon/shadowland/freedumbytes/swingx/demo/Mondriaan.gif");

    final JModalFrame mainFrame = new TestModalFrame("Test Modal Main Frame");
    Window firstDemoFrame = new TestDemoFrame("First Demo Frame", " to demonstrate addAdditionalModalToWindow");
    secondDemoFrame = new TestDemoFrame("Second Demo Frame", " to demonstrate addAdditionalModalToWindow");

    URL imageURL = mainFrame.getClass().getClassLoader().getResource("nl/demon/shadowland/freedumbytes/swingx/demo/CVS logo.gif");
    Window splashWindow = new JSplashWindow(imageURL);
    splashWindow.setBackground(BACKGROUND_COLOR);

    Frame splashFrame = new JSplashFrame(imageURL);
    splashFrame.setBackground(BACKGROUND_COLOR);
    splashFrame.setTitle("Splash Frame");

    Rectangle screenBounds = firstDemoFrame.getGraphicsConfiguration().getBounds();
    firstDemoFrame.setLocation((int)screenBounds.getX() + 50, (int)screenBounds.getY() + 400);
    firstDemoFrame.show();

    screenBounds = secondDemoFrame.getGraphicsConfiguration().getBounds();
    secondDemoFrame.setLocation((int)screenBounds.getX() + 500, (int)screenBounds.getY() + 400);
    secondDemoFrame.show();

    screenBounds = splashWindow.getGraphicsConfiguration().getBounds();
    splashWindow.setLocation((int)screenBounds.getX() + 700, (int)screenBounds.getY() + 80);
    splashWindow.show();

    screenBounds = splashFrame.getGraphicsConfiguration().getBounds();
    splashFrame.setLocation((int)screenBounds.getX() + 700, (int)screenBounds.getY() + 225);
    splashFrame.show();

    screenBounds = mainFrame.getGraphicsConfiguration().getBounds();
    mainFrame.setLocation((int)screenBounds.getX() + 50, (int)screenBounds.getY() + 50);
    mainFrame.setVisible(true);

    mainFrame.waitForClose();

    System.exit(0);
  }
}
