package nl.demon.shadowland.freedumbytes.swingx.sample;


import java.awt.*;
import java.awt.event.*;
import java.net.*;

import javax.swing.*;

import nl.demon.shadowland.freedumbytes.swingx.gui.*;
import nl.demon.shadowland.freedumbytes.swingx.gui.modal.*;
import nl.demon.shadowland.freedumbytes.swingx.gui.utility.*;


/**
 * <p>The sample JModalFrame used for the <a href="https://freedumbytes.gitlab.io/jmodalwindow.xhtml">java.net article</a>. </p>
 * <p>Project: JModalWindow - window-specific modality. </p>
 * <p>Copyright: Copyright (c) 2001-2006. </p>
 *
 * <p>This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version. </p>
 *
 * <p>This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. </p>
 *
 * <p>You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA </p>
 *
 * @author Jene Jasper
 * @version 2.0
 */
public class SampleModalFrame extends JModalFrame
{
  private static final long serialVersionUID = 1L;

  private static final Color BACKGROUND_COLOR = new Color(48, 48, 152);
  private static final Color FOREGROUND_COLOR = new Color(184, 188, 240);

  public SampleModalFrame(String title)
  {
    super(title);

    init();
  }


  public SampleModalFrame(Window owner, Component returnFocusComponent, String title)
  {
    super(owner, returnFocusComponent, title);

    init();
  }


  private void init()
  {
    URL imageURL = getClass().getClassLoader().getResource("nl/demon/shadowland/freedumbytes/swingx/sample/CVS logo.gif");
    JPanel jpnl = new JImagePanel(imageURL);

    setSize(600, 300);
    setMinSize(450, 300);

    setContentPane(jpnl);
    getContentPane().setLayout(new GridBagLayout());

    getContentPane().setBackground(BACKGROUND_COLOR);
    getContentPane().setForeground(FOREGROUND_COLOR);

    GridBagConstraints gbc = new GridBagConstraints();

    final JButton jbCommitWindow = createJButton("Commit");
    jbCommitWindow.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        createStatusWindow(jbCommitWindow, true, "      Committing changes to repository").setVisible(true);
      }
    });

    final JButton jbUpdate = createJButton("Update");
    jbUpdate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        createStatusWindow(jbUpdate, true, "        Updating source with repository changes").setVisible(true);
      }
    });

    final JButton jbRevert = createJButton("Revert");
    jbRevert.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        createStatusWindow(jbRevert, false, "       Reverting changes to source").setVisible(true);
      }
    });

    gbc.gridy = 0;
    gbc.gridx = GridBagConstraints.RELATIVE;
    gbc.insets = new Insets(3, 3, 3, 3);
    gbc.fill = GridBagConstraints.NONE;

    getContentPane().add(createLabel("Source:"), gbc);
    getContentPane().add(createLabel("JModalWindow.java"), gbc);
    getContentPane().add(createLabel("text"), gbc);

    gbc.gridy++;
    getContentPane().add(createLabel("Revision:"), gbc);
    getContentPane().add(createLabel("1.2"), gbc);
    getContentPane().add(createLabel("2003/10/16 10:17:00"), gbc);

    gbc.gridy++;
    getContentPane().add(createLabel("Changed:"), gbc);
    getContentPane().add(createLabel("Locally"), gbc);
    getContentPane().add(createLabel("2004/03/03 19:37:56"), gbc);

    gbc.gridy++;
    getContentPane().add(jbCommitWindow, gbc);
    getContentPane().add(jbUpdate, gbc);
    getContentPane().add(jbRevert, gbc);
  }


  private JModalWindow createStatusWindow(Component returnFocusComponent, boolean relative, String text)
  {
    Window owner = Utils.windowForComponent(returnFocusComponent);
    final JModalWindow jmw = new JModalWindow(owner, returnFocusComponent);
    jmw.getContentPane().setBackground(BACKGROUND_COLOR);
    jmw.getContentPane().setForeground(FOREGROUND_COLOR);
    jmw.getContentPane().setLayout(new GridBagLayout());

    final JButton jbClose = createJButton("Cancel");
    jbClose.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ae)
      {
        jmw.dispose();
      }
    });

    BorderLayout bl = new BorderLayout();
    jmw.getContentPane().setLayout(bl);

    jmw.getContentPane().add(createLabel(text), BorderLayout.CENTER);
    jmw.getContentPane().add(jbClose, BorderLayout.SOUTH);

    jmw.setSize(300, 75);

    if (relative)
    {
      jmw.relativeToOwnerChild(returnFocusComponent);
    }
    else
    {
      jmw.centerOfOwner();
//      jmw.centerOfScreen();  //another positioning possibility
    }

    return jmw;
  }


  private static JLabel createLabel(String text)
  {
    JLabel label = new JLabel(text);
    label.setForeground(FOREGROUND_COLOR);
    label.setBackground(BACKGROUND_COLOR);

    return label;
  }


  private static JButton createJButton(String text)
  {
    return new JButton(text);
  }


  public static void main(String[] args)
  {
    final JModalFrame mainFrame = new SampleModalFrame("CVS Client");
    Rectangle screenBounds = mainFrame.getGraphicsConfiguration().getBounds();

    mainFrame.setLocation((int)screenBounds.getX() + 50, (int)screenBounds.getY() + 50);
    mainFrame.setVisible(true);
    mainFrame.waitForClose();

    System.exit(0);
  }
}
